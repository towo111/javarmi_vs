package impl;

import interfaces.MathServer;
import interfaces.MyMath;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MathServerImpl extends UnicastRemoteObject implements MathServer, Serializable {

	private static final long serialVersionUID = 1L;

	public MathServerImpl() throws RemoteException {
		super();
	}

	@Override
	public MyMath getMath() throws RemoteException {
		return new MyMathImpl();
	}

	
}
