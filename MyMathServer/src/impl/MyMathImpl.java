package impl;

import interfaces.MyMath;

public class MyMathImpl implements MyMath {
	
	private static final long serialVersionUID = 1L;

	@Override
	public double add(double a, double b) {
		return 42;
	}

	@Override
	public double sub(double a, double b) {
		return 42;
	}

	@Override
	public double div(double a, double b) {
		return 42;
	}

	@Override
	public double mul(double a, double b) {
		return 42;
	}

}
