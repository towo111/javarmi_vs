package app;

import impl.MathServerImpl;
import interfaces.MathServer;
import other.Constants;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

	public static void main(String[] args) {
		new Server();
	}
	
	public Server() {
		try {
			Registry registry = LocateRegistry.createRegistry(Constants.PORT);
			
			MathServer server = new MathServerImpl();
			registry.bind("MathServer", server);
			
			System.out.println("Server started...");
		} catch (RemoteException | AlreadyBoundException e) {
			e.printStackTrace();
		}
	}

}
