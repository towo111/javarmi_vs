package gui;

import javax.swing.JFrame;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;

import app.Calculator;

public class CalculatorWindow extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JTextField inFirst;
	private JTextField inSecond;
	private JLabel lblResult;
	private JComboBox<String> comboBox;
	private Calculator calc;
	
	public CalculatorWindow(Calculator calc) {
		this.calc = calc;
		
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		inFirst = new JTextField();
		getContentPane().add(inFirst);
		inFirst.setColumns(10);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"+", "-", "x", "/"}));
		getContentPane().add(comboBox);
		
		inSecond = new JTextField();
		getContentPane().add(inSecond);
		inSecond.setColumns(10);
		
		JButton btn = new JButton("=");
		btn.addActionListener(this);
		getContentPane().add(btn);
		
		lblResult = new JLabel("");
		getContentPane().add(lblResult);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			lblResult.setText("" + calc.calc(Double.valueOf(inFirst.getText()).doubleValue(), (String) comboBox.getSelectedItem(), Double.valueOf(inSecond.getText()).doubleValue()));
		} catch (Exception ex) {
			lblResult.setText("ERR");
			ex.printStackTrace();
		}
	}
	
}
