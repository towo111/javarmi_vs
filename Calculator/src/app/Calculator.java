package app;

import gui.CalculatorWindow;
import interfaces.MathServer;
import interfaces.MyMath;
import other.Constants;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Calculator {
	
	public static void main(String[] args) {
		Calculator calc = new Calculator();
		new CalculatorWindow(calc);
	}
	
	public static final String host = "localhost";
	
	public double calc(double a, String operator, double b) throws MalformedURLException, RemoteException, NotBoundException {
		MyMath math = fetchMath();
		
		return calc(math, a, operator, b);
	}
	
	private double calc(MyMath math, double a, String operator, double b) {
		if (math == null) return 0;
		
		switch (operator) {
		case "+": 
			return math.add(a, b);
		case "-":
			return math.sub(a, b);
		case "/":
			return math.div(a, b);
		case "x": 
			return math.mul(a, b);
		default:
			return 0;
		}
	}
	
	private MyMath fetchMath() throws MalformedURLException, RemoteException, NotBoundException {
		String registerURI = "rmi://" + host + ":" + Constants.PORT;
		
		MathServer mathServer = (MathServer) Naming.lookup(registerURI + "/MathServer");
		return mathServer.getMath();
	}
}
