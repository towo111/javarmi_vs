package app;

import gui.ServerWindow;
import impl.ClassificatorImpl;
import impl.EvaluationImpl;
import impl.NaiveBayesImpl;
import interfaces.Evaluation;
import interfaces.NaiveBayes;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import other.Constants;

public class Application {

	public static void main(String[] args) {
		ServerWindow serverWindow = new ServerWindow();
		
		new Application(serverWindow);
	}
	
	public Application(ServerWindow serverWindow) {
		try {
			Registry registry = LocateRegistry.createRegistry(Constants.PORT);
			System.out.println("Get Registry.");
			
			// Bind evaluation
			Evaluation eval = new EvaluationImpl();
			registry.rebind("Evaluation", eval);
			System.out.println("Bind Evaluation.");
			
			// Bind NaiveBayes
			NaiveBayes nb = new NaiveBayesImpl();
			registry.rebind("NaiveBayes", nb);
			System.out.println("Bind NaiveBayes.");
			
			// Bind Classificator
			ClassificatorImpl classificator = new ClassificatorImpl(serverWindow);
			registry.rebind("Classificator", classificator);
			System.out.println("Bind Classificator.");
			
			System.out.println("Server started.");
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
