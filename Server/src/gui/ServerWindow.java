package gui;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

public class ServerWindow extends JFrame {

	private static final long serialVersionUID = 1L;
//	private TextArea sentenceTxt;
//	private JButton negBtn;
//	private JButton neutBtn;
//	private JButton posBtn;

	public ServerWindow() {
		super("Application");
		
//		setSize(300, 500);
		setBounds(600, 0, 400, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		initWindow();

		setVisible(true);
	}

	private void initWindow() {
		JLabel lbl = new JLabel("Classificator");
		lbl.setBorder(new EmptyBorder(2, 2, 2, 2));
		lbl.setFont(new Font("ComicSans", Font.BOLD, 15));
		add(lbl, BorderLayout.NORTH);
		
		JLabel lbl1 = new JLabel("Waiting for sentences to classify...");
		add(lbl1, BorderLayout.CENTER);

//		sentenceTxt = new TextArea(3, 40);
//		sentenceTxt.setEditable(false);
//		add(sentenceTxt, BorderLayout.CENTER);
//
//		// Buttons panel
//		JPanel buttonsPanel = new JPanel();
//		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 4));
//		add(buttonsPanel, BorderLayout.SOUTH);
//
//		negBtn = new JButton("Negative");
//		negBtn.addActionListener(this);
//		buttonsPanel.add(negBtn);
//
//		neutBtn = new JButton("Neutral");
//		neutBtn.addActionListener(this);
//		buttonsPanel.add(neutBtn);
//
//		posBtn = new JButton("Positive");
//		posBtn.addActionListener(this);
//		buttonsPanel.add(posBtn);

	}

	public int classify(String input) {
//		JOptionPane.showOptionDialog(parentComponent, message, title, optionType, messageType, icon, options, initialValue)
		
		switch (JOptionPane.showOptionDialog(this, input, "Classify",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, new Object[] {"Positive", "Negative", "Neutral" }, "Positive")) {
			case JOptionPane.YES_OPTION:
				return 1;
			case JOptionPane.NO_OPTION:
				return -1;
			case JOptionPane.CANCEL_OPTION:
				return 0;
			default:
				return 0;
		}

	}

}
