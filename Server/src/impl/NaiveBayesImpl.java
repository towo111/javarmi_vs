package impl;

import interfaces.NaiveBayes;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class NaiveBayesImpl extends UnicastRemoteObject implements NaiveBayes {
	
	private static final long serialVersionUID = 1L;

	public NaiveBayesImpl() throws RemoteException {
		super();
	}

	@Override
	public void buildClassifier(String trainingData) throws RemoteException {
		// Do nothing.
		System.out.println("Build Classifier called");
	}

}
