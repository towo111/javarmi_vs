package impl;

import interfaces.Evaluation;
import interfaces.NaiveBayes;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Formatter;
import java.util.Locale;
import java.util.Random;

public class EvaluationImpl extends UnicastRemoteObject implements Evaluation {
	private static final long serialVersionUID = 1L;

	public EvaluationImpl() throws RemoteException {
		super();
	}

	@Override
	public void crossValidateModel(NaiveBayes nb, String data)
			throws RemoteException {
		// Just do nothing.
		System.out.println("CrossValidateModel called");
	}

	@Override
	public String toSummaryString() throws RemoteException {
		String summary = "==== Summary ====\r\n";
		summary += "Kappa statistic \t\t" + (new Random().nextDouble()) + "\r\n";
		summary += "Mean absolute error \t\t" + (new Random().nextDouble()) + "\r\n";
		summary += "Some random percentage \t\t" + (new Random().nextInt(100)) + " % \r\n";
		summary += "Total Number of Instances \t\t" + (new Random().nextInt(500));
		
		return summary;
	}

	@Override
	public String toClassDetailsString() throws RemoteException {
		return "";
	}

	@Override
	public String toMatrixString() throws RemoteException {
		StringBuilder sb = new StringBuilder();
		sb.append("==== Matrix ====\r\n");
		sb.append("TP Rate\tFP Rate\tPrecision\tClass\r\n");
		
		@SuppressWarnings("resource")
		Formatter formatter = new Formatter(sb, Locale.GERMANY);
		formatter.format("%.2f\t%.2f\t%.2f\t%s\r\n", new Random().nextDouble(), new Random().nextDouble(), new Random().nextDouble(), "Positive");
		formatter.format("%.2f\t%.2f\t%.2f\t%s\r\n", new Random().nextDouble(), new Random().nextDouble(), new Random().nextDouble(), "Negative");
		formatter.format("%.2f\t%.2f\t%.2f\t%s\r\n", new Random().nextDouble(), new Random().nextDouble(), new Random().nextDouble(), "Neutral");
		
		return sb.toString();
	}

}
