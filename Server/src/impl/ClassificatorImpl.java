package impl;

import gui.ServerWindow;
import interfaces.Classificator;
import interfaces.NaiveBayes;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ClassificatorImpl extends UnicastRemoteObject implements
		Classificator {

	private static final long serialVersionUID = 1L;
	private ServerWindow window;
	
	public ClassificatorImpl(ServerWindow window) throws RemoteException {
		super();
		
		this.window = window;		
	}

	@Override
	public int classify(NaiveBayes nb, String input) throws RemoteException {
		return window.classify(input);
	}

}
