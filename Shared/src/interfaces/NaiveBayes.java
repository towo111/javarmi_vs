package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NaiveBayes extends Remote {
	
	public void buildClassifier(String trainingData) throws RemoteException;
}
