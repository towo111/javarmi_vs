package interfaces;

import java.io.Serializable;


public interface MyMath extends Serializable {

	public double add(double a, double b);
	
	public double sub(double a, double b);
	
	public double div(double a, double b);
	
	public double mul(double a, double b);
}
