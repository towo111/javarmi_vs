package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Classificator extends Remote {
	
	public int classify(NaiveBayes nb, String input) throws RemoteException;
}
