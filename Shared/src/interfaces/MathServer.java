package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MathServer extends Remote {

	public MyMath getMath() throws RemoteException;
	
}
