package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Evaluation extends Remote {

	public void crossValidateModel(NaiveBayes nb, String data) throws RemoteException;
	public String toSummaryString() throws RemoteException;
    public String toClassDetailsString() throws RemoteException;
    public String toMatrixString() throws RemoteException;
}
