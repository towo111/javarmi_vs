package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class BackgroundPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static final String imgSrc = "images/hcm.png";

	private BufferedImage backgroundImg;
	
	public BackgroundPanel(Color backgroundColor) {
		this.setBackground(backgroundColor);
		
		try {
			backgroundImg = ImageIO.read(new File(imgSrc));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int imgWidth = Math.min(getWidth(), backgroundImg.getWidth());
		int imgHeight = (int) ((double) backgroundImg.getHeight() / backgroundImg.getWidth() * imgWidth);
		
		int x = (getWidth() - imgWidth) / 2;
		int y = (getHeight() - imgHeight) / 2 - 30;
		
//		System.out.println(getBackground() + " | " + imgWidth + " | " + imgHeight + " | " + x + " | " + y + " | " + backgroundImg.getWidth() + " | " + backgroundImg.getHeight());
		
		g.drawImage(backgroundImg, x, y, imgWidth, imgHeight, getBackground(), null);
	}

}
