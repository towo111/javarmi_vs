package gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class EmotionPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public static final int POSITIVE_EMOTION = 1;
	public static final int NEUTRAL_EMOTION = 0;
	public static final int NEGATIVE_EMOTION = -1;
	
	private static final String srcPositive = "images/positive.png";
	private static final String srcNeutral = "images/neutral.png";
	private static final String srcNegative = "images/negative.png";
	
	private BufferedImage imgPositive;
	private BufferedImage imgNeutral;
	private BufferedImage imgNegative;
	
	private int emotion = 0;
	
	public EmotionPanel() {
		try {
			imgPositive = ImageIO.read(new File(srcPositive));
			imgNeutral = ImageIO.read(new File(srcNeutral));
			imgNegative = ImageIO.read(new File(srcNegative));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setEmotion(int emotion) {
		this.emotion = emotion;
	}
	
	public void setPositiveEmotion() {
		emotion = POSITIVE_EMOTION;
	}
	
	public void setNeutralEmotion() {
		emotion = NEUTRAL_EMOTION;
	}
	
	public void setNegativeEmotion() {
		emotion = NEGATIVE_EMOTION;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		switch (emotion) {
		case POSITIVE_EMOTION:
			g.drawImage(imgPositive, 0, 0, getWidth(), getHeight(), null);
			break;
		case NEGATIVE_EMOTION:
			g.drawImage(imgNegative, 0, 0, getWidth(), getHeight(), null);
			break;
		default:
			g.drawImage(imgNeutral, 0, 0, getWidth(), getHeight(), null);
			break;
		}
	}

}
