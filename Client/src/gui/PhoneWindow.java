package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import app.Application;

public class PhoneWindow extends JFrame implements ActionListener, KeyListener {
	
	private static final long serialVersionUID = 1L;
	private JButton trainBtn;
	private JButton testBtn;
	private Application app;
	private JTextArea outputText;
	private JTextField inputTxt;
	private EmotionPanel emotionPanel;

	public PhoneWindow(Application app) {
		super("Application");
		
		this.app = app;
		
		setSize(500, 700);
		this.setBackground(new Color(49, 102, 24));	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(new BackgroundPanel(new Color(49, 101, 24)));

		initWindow();
		
		repaint();
		
		setVisible(true);
	}
	
	private void initWindow() {
		getContentPane().setLayout(new BorderLayout());
		
		// Top panel
		JPanel topPanel = new JPanel();
		topPanel.setBorder(new EmptyBorder(0, 0, 60, 0));
		topPanel.setSize(getWidth(), 40);
//		topPanel.setBackground(new Color(Color.TRANSLUCENT));
		topPanel.setOpaque(false);
		getContentPane().add(topPanel, BorderLayout.NORTH);
		
		GridBagLayout gbl_topPanel = new GridBagLayout();
		gbl_topPanel.columnWidths = new int[]{15, 80, 227, 150, 0, 0};
		gbl_topPanel.rowHeights = new int[]{18, 69, 70, 0};
		gbl_topPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_topPanel.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		topPanel.setLayout(gbl_topPanel);
		
		// Add buttons on top left
		trainBtn = new JButton("Train");
		GridBagConstraints gbc_trainBtn = new GridBagConstraints();
		gbc_trainBtn.fill = GridBagConstraints.BOTH;
		gbc_trainBtn.insets = new Insets(0, 0, 5, 5);
		gbc_trainBtn.gridx = 1;
		gbc_trainBtn.gridy = 1;
		topPanel.add(trainBtn, gbc_trainBtn);
		trainBtn.addActionListener(this);
		
		emotionPanel = new EmotionPanel();
		emotionPanel.setOpaque(false);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 2;
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 1;
		topPanel.add(emotionPanel, gbc_panel);
		
		testBtn = new JButton("Test");
		GridBagConstraints gbc_testBtn = new GridBagConstraints();
		gbc_testBtn.fill = GridBagConstraints.BOTH;
		gbc_testBtn.insets = new Insets(0, 0, 0, 5);
		gbc_testBtn.gridx = 1;
		gbc_testBtn.gridy = 2;
		topPanel.add(testBtn, gbc_testBtn);
		testBtn.addActionListener(this);
		
		// Center panel
		JPanel centerPanel = new JPanel();
		centerPanel.setOpaque(false);
		getContentPane().add(centerPanel, BorderLayout.CENTER);
		
		// Add input textfield
		inputTxt = new JTextField(40);
		inputTxt.setOpaque(true);
		inputTxt.addKeyListener(this);
		centerPanel.add(inputTxt);
		
		
		// Bottom panel
		JPanel bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		getContentPane().add(bottomPanel, BorderLayout.SOUTH);
		
		outputText = new JTextArea(20, 40);
		outputText.setEditable(false);
		outputText.setBorder(null);
		outputText.setOpaque(false);
		bottomPanel.add(outputText);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == trainBtn) {
			outputText.setText(app.train());
		} else if (e.getSource() == testBtn) {
			int emotion = app.test(inputTxt.getText());
			emotionPanel.setEmotion(emotion);
			repaint();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}
