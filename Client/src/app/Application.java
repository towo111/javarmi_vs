package app;

import gui.PhoneWindow;
import interfaces.Classificator;
import interfaces.Evaluation;
import interfaces.NaiveBayes;

import java.rmi.Naming;
import java.rmi.RemoteException;

import other.Constants;

public class Application {

	public static void main(String[] args) {			
		Application app = new Application(args.length > 0 ? args[0] : Application.host);
		
		new PhoneWindow(app);
	}

	private Evaluation eval;
	private NaiveBayes nb;
	private Classificator classificator;
	
	public static final String host = "localhost";
	
	public Application(String host) {
		String registerURI = "rmi://" + host + ":" + Constants.PORT;		
		
		try {			
			eval = (Evaluation) Naming.lookup(registerURI + "/Evaluation");
			nb = (NaiveBayes) Naming.lookup(registerURI + "/NaiveBayes");
			classificator = (Classificator) Naming.lookup(registerURI + "/Classificator");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String train() {
		try {
			eval.crossValidateModel(nb, "");
			return eval.toSummaryString() + "\r\n\r\n" + eval.toMatrixString();
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return "";
	}

	public int test(String input) {
		int classify;
		try {
			classify = classificator.classify(nb, input);
			
			if (classify < 0) {
				return -1;
			} else if (classify > 0) {
				return 1;
			} else {
				return 0;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return 0;
	}

}
